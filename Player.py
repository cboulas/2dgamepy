import pygame

class Player(pygame.sprite.Sprite):
    def __init__(self, x, y):
        #Appel du constructeur Base
        super().__init__()
        #Chargement de l'image Sprites joueur
        self.sprite_sheet = pygame.image.load("poke2.png")
        # Définition du code image de base
        self.imagekey = 0
        #Récupération de l'image tout en haut à gauche
        self.image = self.get_image(0, self.imagekey)
        #Définition de la couleur de transparence
        self.image.set_colorkey([0, 0, 0])
        #Définition de la zone de dessin
        self.rect = self.image.get_rect()
        #Définition de la position au départ
        self.position = [x, y]
        #Définition de la vitesse de déplacement
        self.speed = 3
        #Définition des coordonnées des pieds
        self.feet = pygame.Rect(0, 0, self.rect.width * .5, 12)
        #Conserve la position précédente
        self.old_position = self.position.copy()
        #Définition de l'inventaire
        self.inventory = []

    def save_location(self): self.old_position = self.position.copy()

    def set_speed(self, _speed):
        self.speed = _speed

    def move_right(self):
        self.position[0] += self.speed
        self.imagekey = 2

    def move_left(self):
        self.position[0] -= self.speed
        self.imagekey = 1

    def move_up(self):
        self.position[1] -= self.speed
        self.imagekey = 3

    def move_down(self):
        self.position[1] += self.speed
        self.imagekey = 0

    def update(self):
        #Mise à jour de la position joueur
        self.rect.topleft = self.position
        #Récupération de l'image
        self.image = self.get_image(0, self.imagekey)
        #Définition de la couleur de transparence
        self.image.set_colorkey([0, 0, 0])
        #Récupération de la position des pieds
        self.feet.midbottom = self.rect.midbottom

    def move_back(self):
        # Récupération de l'ancienne position
        self.position = self.old_position.copy()
        # Mise à jour de la position joueur
        self.rect.topleft = self.position
        # Récupération de la position des pieds
        self.feet.midbottom = self.rect.midbottom

    def get_image(self, x, y):
        #Définition de l'image
        image = pygame.Surface([32, 32])
        #Récupération du Sprite correspondant
        image.blit(self.sprite_sheet, (0, 0), (x * 32, y * 32, 32, 32))
        #Renvoi de l'image
        return image

    def UseObject(self, _object):
        _object.Use(self)

    def showInventory(self):
        if len(self.inventory) == 0:
            print("Inventaire vide")

        for item in self.inventory:
            item.ShowItem()