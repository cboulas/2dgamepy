import distutils.util

import pygame
import pytmx
import pyscroll

from House import House
from InventoryItem import InventoryItem
from Player import Player


class Game:
    #Constructeur
    def __init__(self):
        #Créer la fenêtre
        self.setWindow()

        #Chargement de la carte de base
        self.switch_map("MyMap", "startpos_player")

    def setWindow(self):
        # Création avec une taille
        self.screen = pygame.display.set_mode((800, 600))
        # Définition du titre
        pygame.display.set_caption("2DGame")

    def loadMap(self, id):
        #Charger la carte
        self.tmx_data = pytmx.util_pygame.load_pygame(id + ".tmx")
        #Rafraichit l'affichage, ici affichage principal
        self.refreshMap()
        # Definition du nom de la carte
        self.currentMap = id

    def refreshMap(self):
        # Chargement des tiles
        self.map_data = pyscroll.data.TiledMapData(self.tmx_data)
        # Chargement des calques
        self.map_layers = pyscroll.orthographic.BufferedRenderer(self.map_data, self.screen.get_size())
        # Affection du zoom sur la carte (pas inférieur à 0)
        self.map_layers.zoom = 2

    def setPlayer(self, spawnposition: "startpos_player"):
        #Générer un joueur
        # On récupère la position de l'objet startpos_player situé dans la carte
        player_position = self.tmx_data.get_object_by_name(spawnposition)

        # Instanciation avec position du joueur
        if not hasattr(self, "player"): self.player = Player(player_position.x, player_position.y)

    def defineCollision(self):
        self.walls = []
        self.currentCollision = None

    def defineIce(self):
        self.ice = []

    def defineObject(self):
        self.objectsMap = []
        self.objectsMap_rects = []

    def defineHouses(self):
        self.houses = []
        self.houses_rect = []

    def loadMapElements(self):
        #Pour chaque objets dans le tmx
        for obj in self.tmx_data.objects:
            currentRect = pygame.Rect(obj.x, obj.y, obj.width, obj.height)

            #si c'est du type Collision
            if obj.type == "collision":
                #On ajoute à notre liste de murs selon le rectangle x, y, W, H
                self.walls.append(currentRect)

            if obj.type == "iceland":
                # On ajoute à notre liste de glaces selon le rectangle x, y, W, H
                self.ice.append(currentRect)

            if obj.type == "objectsmap":
                self.objectsMap_rects.append(currentRect)

                # On ajoute à notre liste de glaces selon le rectangle x, y, W, H
                self.objectsMap.append(
                    InventoryItem(
                        obj.name,
                        obj.gametype,
                        currentRect,
                        obj.title,
                        obj,
                        obj.actcode,
                        obj.usecode,
                        distutils.util.strtobool(obj.collision)
                    )
                )

            if obj.type == "housesdoor":
                self.houses_rect.append(currentRect)

                # On ajoute à notre liste de glaces selon le rectangle x, y, W, H
                self.houses.append(
                    House(
                        obj.name,
                        currentRect,
                        obj.mapTarget,
                        obj.playerspawn)
                )

    def drawMapAndPlayer(self, defaultLayerLevel: 6):
        # Dessiner les calques (En cas de pb d'affichage, il faudra faire monter le niveau de casque Default_Layer
        self.group = pyscroll.PyscrollGroup(map_layer=self.map_layers, default_layer=defaultLayerLevel)
        # On ajoute un calque correspondant au joueur
        self.group.add(self.player)

    def switch_map(self, mapId, positionTag: "startpos_player"):
        #Chargement de la carte de base
        self.loadMap(mapId)

        #Définition des items de la carte
        self.defineCollision()
        self.defineIce()
        self.defineObject()
        self.defineHouses()
        self.loadMapElements()

        # Création du joueur
        self.setPlayer(positionTag)

        # Dessin de la zone de jeu
        self.drawMapAndPlayer(6)

        # On récupère la position de l'objet startpos_player situé dans la carte
        player_position = self.tmx_data.get_object_by_name(positionTag)
        self.player.position = [player_position.x, player_position.y]

    #Méthode de gestion des touches clavier
    def handle_input(self):
        #Récupération de la touche pressée
        pressed = pygame.key.get_pressed()

        if pressed[pygame.K_UP]:
            self.player.move_up()
        elif pressed[pygame.K_DOWN]:
            self.player.move_down()
        elif pressed[pygame.K_LEFT]:
            self.player.move_left()
        elif pressed[pygame.K_RIGHT]:
            self.player.move_right()
        elif pressed[pygame.K_SPACE]:
            print("attaque not implemented")
        elif pressed[pygame.K_RETURN]:
            if self.currentCollision != None:
                self.player.UseObject(self.currentCollision)

               # if self.currentCollision.Object in self.tmx_data:
               #     try:
               #         self.tmx_data.get_object_by_name(self.currentCollision.Name).__setattr__("visible", 0)
               #         self.tmx_data.get_object_by_name(self.currentCollision.Name).apply_transformations()
               #     except:
               #         quit()
        elif pressed[pygame.K_i]:
            self.player.showInventory()

    def update(self):
        self.group.update()

        #Vérification de la collision
        for sprite in self.group.sprites():
            sprite.set_speed(3)

            if sprite.feet.collidelist(self.walls) > -1:
                sprite.move_back()

            if sprite.feet.collidelist(self.houses_rect) > -1:
                for house in self.houses:
                    # Vérification de l'entée dans la maison
                    if self.player.feet.colliderect(house.Position):
                        sprite.move_back()
                        self.switch_map(
                            house.Map,
                            house.StartPosition)
                        break

            if sprite.feet.collidelist(self.objectsMap_rects) > -1:
                for objs in self.objectsMap:
                    if sprite.feet.colliderect(objs.Position):
                        if bool(objs.Collision): sprite.move_back()
                        self.currentCollision = objs
                        break

            if sprite.feet.collidelist(self.ice) > -1:
                sprite.set_speed(1)

    #Méthode de lancement de l'appli
    def run(self):
        #Définition de la vitesse de jeu
        clock = pygame.time.Clock()

        # boucle du hjeu pour maintenir alive la fenêtre
        running = True

        #Tant que l'app tourne (grace à running)
        while running:
            #Sauvegarde par sécurité la position du joueur
            self.player.save_location()
            #Récupération des events clavier
            self.handle_input()
            # Mise à jour des calques via les méthodes mère et héritée Update
            self.update()
            # Centrage par rapport au joueur
            self.group.center(self.player.rect)
            # Dessin des objets en mémoire
            self.group.draw(self.screen)
            # Affichage des objets - Quid du "flip()"
            pygame.display.flip()

            #Récupération des évênements de l'appli
            for _event in pygame.event.get():
                # Si l'event est de quitter
                if _event.type == pygame.QUIT:
                    # On défini running à False ce qui permet de brisé la boucle
                    running = False

            #Après chaque évènement de touche clavier l'on attends 60ms
            clock.tick(60)

        #On quitte l'appli
        pygame.quit()
