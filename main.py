import pygame

from Game import Game

if __name__ == '__main__':
    #Initialisaiton obligatoire de pygame -- Once dans l'appli
    pygame.init()
    #Instanciation du jeu
    game = Game()
    #Lancement du jeu
    game.run()
