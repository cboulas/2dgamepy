class InventoryItem:
    def __init__(self, _name, _type, _position, _title, _object, _actCode, _useCode, _collision):
        self.Name = _name
        self.Type = _type
        self.Object = _object
        self.Position = _position
        self.Title = _title
        self.ActionCode = _actCode
        self.UseCode = _useCode
        self.Collision = _collision

    def ShowItem(self):
        print(self.Name + " : " + self.Object)

    def Action(self, player):
        #player.inventory.append(self)
        print("do nothing")

    def Use(self, player):
        if self.UseCode == "sleep":
            print(self.Title + " : Bonne nuit")
        elif self.UseCode == "read":
            print(self.Title + " : " + self.Object.message)
        elif self.UseCode == "eatthis":
            print(self.Object.message)
